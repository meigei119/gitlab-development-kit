# GitLab AI Gateway

You can configure the [GitLab AI Gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist) to run locally in GDK, which automates the
**Setup GitLab Development Kit (GDK)** step in [Local setup](https://docs.gitlab.com/ee/development/ai_features/index.html#local-setup).

To configure:

1. Run `gdk config set gitlab_ai_gateway.enabled true`.
1. Run `gdk update`.
1. Run `gdk start gitlab-ai-gateway`.
1. Go to the [GitLab AI Gateway API docs page](http://localhost:5052/docs) to verify that the GitLab AI Gateway started.

For more information, see the [AI features based on 3rd-party integrations](https://docs.gitlab.com/ee/development/ai_features/index.html) page.
